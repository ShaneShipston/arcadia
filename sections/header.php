<header class="header" role="banner">
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
        </a>

        <nav class="navigation" role="navigation" aria-label="site">
            <?php wp_nav_menu([
                'theme_location' => 'primary',
                'depth' => 2,
            ]); ?>
        </nav>
    </div>
</header>
