<footer class="footer" role="contentinfo">
    <div class="container">
        <nav class="nav" aria-label="site">
            <?php wp_nav_menu([
                'theme_location' => 'secondary',
                'depth' => 1,
            ]); ?>
        </nav>

        <?php if (get_field('social_platforms', 'option')) : ?>
            <div class="social">
                <?php Layout::partial('social'); ?>
            </div>
        <?php endif; ?>

        <div itemscope itemtype="http://schema.org/LocalBusiness" class="address">
            <?php if (get_field('locations', 'option')) : ?>
                <?php foreach (get_field('locations', 'option') as $location) :
                    if (!$location['default']) :
                        continue;
                    endif;
                    ?>
                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <?php if ($location['address']) : ?>
                            <span itemprop="streetAddress"><?php echo $location['address']; ?></span><br>
                        <?php endif; ?>
                        <?php if ($location['city']) : ?>
                            <span itemprop="addressLocality"><?php echo $location['city']; ?></span>,
                        <?php endif; ?>
                        <?php if ($location['province']) : ?>
                            <span itemprop="addressRegion"><?php echo $location['province']; ?></span>
                        <?php endif; ?>
                        <?php if ($location['postal_code']) : ?>
                            <span itemprop="postalCode"><?php echo $location['postal_code']; ?></span>
                        <?php endif; ?>
                    </p>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if (get_field('phone_numbers', 'option')) :
                foreach (get_field('phone_numbers', 'option') as $phone) :
                    if (!$phone['default']) :
                        continue;
                    endif; ?>
                    <?php echo $phone['label']; ?><span itemprop="<?php echo $phone['type'] == 'faxNumber' ? $phone['type'] : 'telephone'; ?>"><?php echo $phone['number']; ?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</footer>
